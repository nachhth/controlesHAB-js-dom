'use strict';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max + 1 - min) + min);
}

let minNum = 10;
let maxNum = 20;

const getPrompt = () => {
  let isCorrect = true;
  let userElection;
  do {
    userElection = parseInt(prompt(`Choose a number between ${minNum} and ${maxNum}.`));
    if (userElection < minNum || userElection > maxNum) {
      alert(`Your number is not between ${minNum} and ${maxNum}. Try again.`);
      isCorrect = false;
    } else if (isNaN(userElection)) {
      alert(`You didn't enter a number. Try again.`);
      isCorrect = false;
    } else {
      isCorrect = true;
    }
  } while (!isCorrect);

  return userElection;
};

let password = getRandomInt(minNum, maxNum);
console.log(password);

let trys = 5;

while (trys !== 0) {
  let userChoice = getPrompt();
  if (userChoice === password) {
    alert('You won! your number was correct.');
    break;
  } else {
    trys = trys - 1;
    if (trys === 0) {
      alert('Sorry, you lost. No trys left.');
    } else if (password > userChoice) {
      alert(`Wrong, the password is a bigger number. trys left: ${trys}`);
    } else if (password < userChoice) {
      alert(`Wrong, the password is a smaller number. trys left: ${trys}`);
    }
  }
}
