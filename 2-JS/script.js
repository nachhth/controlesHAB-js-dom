'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

function getBestAndWorst(arrayOfTeams) {
  const totalPointsOfEachTeam = [];

  for (const team of arrayOfTeams) {
    let points = team.puntos;
    let sumOfPoints = 0;
    for (const point of points) {
      sumOfPoints = sumOfPoints + point;
    }
    totalPointsOfEachTeam.push(sumOfPoints);
  }

  let pointsOfbestTeam = Math.max(...totalPointsOfEachTeam);
  let pointsOfworstTeam = Math.min(...totalPointsOfEachTeam);

  const sum = (previousValue, currentValue) => previousValue + currentValue;

  const bestTeam = arrayOfTeams.filter((equipo) => {
    let totalPoints = equipo.puntos.reduce(sum);
    return totalPoints > pointsOfbestTeam - 1;
  });

  const worstTeam = arrayOfTeams.filter((equipo) => {
    let totalPoints = equipo.puntos.reduce(sum);
    return totalPoints < pointsOfworstTeam + 1;
  });

  return 'Best team: ' + bestTeam[0].equipo + ' with a total of ' + pointsOfbestTeam + ' points. \nWorst team: ' + worstTeam[0].equipo + ' with a total of ' + pointsOfworstTeam + ' points.';
}

console.log(getBestAndWorst(puntuaciones));
