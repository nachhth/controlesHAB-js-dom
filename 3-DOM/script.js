'use strict';

function getCurrentTime() {
  const currentTime = new Date();

  const options_hour = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  };

  const formatTime = currentTime.toLocaleTimeString('es-ES', options_hour);

  return formatTime;
}

// console.log(getCurrentTime());

const body = document.querySelector('body');
const div = document.createElement('div');
div.setAttribute('class', 'clock');
body.append(div);

setInterval(() => {
  div.textContent = getCurrentTime();
}, 1000);
